package pl.codeleak.samples.redis;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Component
public class TaskRepository {

    private final RestTemplate restTemplate = new RestTemplate();

    public List<Task> findAllTasks() {
        // Simulate slow network connection
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ignored) {
        }

        // Fetch tasks from the remote service
        Task[] tasks = restTemplate.getForObject("https://jsonplaceholder.typicode.com/todos", Task[].class);
        return Arrays.asList(tasks != null ? tasks : new Task[0]);
    }
}
