package pl.codeleak.samples.redis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ObjectMapper objectMapper;

    public List<Task> getTasks() {
        // TODO Modify the method to cache the tasks in Redis for 30 seconds.
        //  If the tasks are found in Redis, return them from the cache.
        //  Otherwise, fetch the tasks from the repository, cache them in Redis, and return them.
        //  Use the toJsonString and getValue methods to convert the tasks to JSON and vice versa.
        return taskRepository.findAllTasks();
    }

    /**
     * Convert list of tasks to JSON string
     */
    String toJsonString(List<Task> tasks) {
        try {
            return objectMapper.writeValueAsString(tasks);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Convert JSON string to list of tasks
     */
    List<Task> getValue(String jsonString) {
        try {
            return objectMapper.readValue(jsonString, new TypeReference<List<Task>>() {
            });
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
