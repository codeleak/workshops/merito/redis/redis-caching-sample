package pl.codeleak.samples.redis;

public record Task(int userId, int id, String title, boolean completed) {
}
