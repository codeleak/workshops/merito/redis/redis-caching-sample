1. Enable caching in `TaskService` using `RedisTemplate` and `ValueOperations` to cache the tasks.
2. Improve the example by using `@Cacheable` annotation to cache the tasks.
    - https://docs.spring.io/spring-boot/reference/io/caching.html
    - https://docs.spring.io/spring-boot/reference/io/caching.html#io.caching.provider.redis
    - https://www.baeldung.com/spring-boot-redis-cache

Steps:

- Start by adding `spring-boot-starter-cache` dependency in `pom.xml`.
- Then uncomment `RedisConfig` class and add `@EnableCaching` annotation in `RedisDemoApplication` class.
- Now, use `@Cacheable` annotation in `TaskService` class.
